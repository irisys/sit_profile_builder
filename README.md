# README

## build\_SIT\_profile.py

**build\_SIT\_profile.py** is a script for building a SIT profile from several parts.

### Preconditions

**SIT\_profile\_global.xml** is a global configs file.  
At the moment, the only relevant property therein is EngineVersion.  
This should live in the same directory as *build\_SIT\_profile.py*

**ProfileCreator.exe** is used primarily to add a checksum to the file.  
It has the nice side-effect of prettifying XML formatting, since Python's ElementTree doesn't do an excellent job.  
This should also live alongside *build\_SIT\_profile.py*  
This is built as part of the SIT solution, elsewhere in GIT. You will also need a dependency, found alongside it:
* Interfaces.WPF.dll

### Usage

`python build_SIT_profile.py -c customer_file [-i inputs_folder]`  
where  
`customer_file` is a customer-specific cut-down SIT profile that also includes a CustomerNumber field, used for generating part numbers.  
`inputs_folder` is a folder which itself contains:  

- A folder named *PartNumberRegistry*, containing XML files for individual parts with genericised part numbers (e.g. IRQ5X35-MW).  
- A folder named *CheckoutProfiles*, containing XML files for individual checkout profiles.  
If this argument is not supplied, these folders will be expected in the same directory as the customer file.  

### Behaviour

The script will build a SIT profile from:  

- The customer file.  
- The global configs file will supply the engine version.  
- The contents of the *PartNumberRegistry* folder - the CustomerNumber element in the customer file will be used to generate specific part numbers.  
- The contents of the *CheckoutProfiles* folder.  

In the customer file, the Version field will be incremented. If the Description field contains an SPF number (e.g. SPF1234567), the last two digits of that will be incremented.  
These new figures will be present in the generated SIT profile.  
This new file will be in the same location as the input customer file, but prepended with the SPF number.  


## split\_SIT\_profile.py

**split\_SIT\_profile.py** is a utility script that does the opposite, in the event that someone wants to use the above script with elements from a pre-existing SIT profile.  

### Usage

`python split_SIT_profile.py -p profile_file`  
where  
`profile_file` is a complete SIT profile.  

### Behaviour

The script will read the profile and create the following, in the same directory as the input profile:

- A customer file, with customer number extracted from part numbers. If the source filename began with an SPF number, the output will use that but with the SPF number removed.  
- A *PartNumberRegistry* folder containing all component PartNumberRegistryEntry elements, with genericised part numbers.  
- A *CheckoutProfiles* folder containing all component CheckoutProfileEntry elements.

## ProfileCreator.exe

This project lives in another repository, but it finds use here for setting checksums.  
If you have made a manual change to a (complete) profile and wish to update the checksum, call:  
`ProfileCreator -addHash inputFile [outputFile]`  
where  
`inputFile` is an exisiting SIT profile file to read in.  
`outputFile` is the desired output - if this already exists, it will be overwritten.  
If `outputFile` is not specified, `inputFile` will be used for both reading and writing.  
