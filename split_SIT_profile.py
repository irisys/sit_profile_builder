from xml.etree import ElementTree
import re
import os
import argparse


__author__ = "InfraRed Integrated Systems Ltd."
__copyright__ = "Copyright (C) 2017, InfraRed Integrated Systems Ltd."
__license__ = "Irisys Software License Agreement"


def make_generic_part_number(part_number):
    if part_number.startswith('IRC'):
        return part_number, ''

    m = re.match(r"(IRQ\w)(\w)(.*)", part_number)
    customer_number = ""
    if m is not None and m.group(1) is not None and m.group(2) is not None and m.group(3) is not None:
        part_number = m.group(1) + "X" + m.group(3)
        customer_number = m.group(2)
    else:
        print("Unable to build part number from: " + part_number)
    return part_number, customer_number


def extract_part_numbers(profile_root, output_folder):
    customer_number = ""
    output_folder = os.path.join(output_folder, "PartNumberRegistry")
    if not os.path.isdir(output_folder):
        os.makedirs(output_folder)
    parent_root = profile_root.find("PartNumberRegistry")
    for child in parent_root:
        output_parent = ElementTree.Element("PartNumberRegistry")
        part_numbers = child.find("PartNumbers")
        for string_val in part_numbers:
            new_part_number, customer_number = make_generic_part_number(string_val.text)
            string_val.text = new_part_number
        output_parent.append(child)
        output_tree = ElementTree.ElementTree(output_parent)
        output_file_name = child.find("RegistryID").text.replace(' ', '_') + ".xml"
        output_file_path = os.path.join(output_folder, output_file_name)
        output_tree.write(output_file_path, encoding="utf-8", xml_declaration=True)

    return customer_number


def extract_checkout_profiles(profile_root, output_folder):
    output_folder = os.path.join(output_folder, "CheckoutProfiles")
    if not os.path.isdir(output_folder):
        os.makedirs(output_folder)
    parent_root = profile_root.find("CheckoutProfiles")
    for child in parent_root:
        output_parent = ElementTree.Element("CheckoutProfiles")
        output_parent.append(child)
        output_tree = ElementTree.ElementTree(output_parent)
        output_file_name = child.find("ProfileID").text.replace(' ', '_') + ".xml"
        output_file_path = os.path.join(output_folder, output_file_name)
        output_tree.write(output_file_path, encoding="utf-8", xml_declaration=True)


def make_output_filename(path):
    file_dir, file_name = os.path.split(path)
    m = re.match(r"SPF[\d]{7}_(.*)", file_name)
    if m:
        file_name = m.group(1)
    else:
        file_name = "base_" + file_name
    return file_name


def split_SIT_profile(input_file):
    profile_tree = ElementTree.parse(input_file)
    profile_root = profile_tree.getroot()
    output_folder = os.path.splitext(input_file)[0]

    customer_number = extract_part_numbers(profile_root, output_folder)

    extract_checkout_profiles(profile_root, output_folder)

    profile_root.remove(profile_root.find("PartNumberRegistry"))
    profile_root.remove(profile_root.find("CheckoutProfiles"))

    profile_root.remove(profile_root.find("Checksum"))

    profile_root.find("Summary").attrib.pop("EngineVersion", None)

    customer_number_element = ElementTree.Element("CustomerNumber")
    if customer_number:
        customer_number_element.text = customer_number
    else:
        print("CustomerNumber not found - you will need to set this manually")
        customer_number_element.text = "?"
        customer_number_element.append(ElementTree.Comment("CustomerNumber should be set before using this file"))
    profile_root.append(customer_number_element)

    output_file = os.path.join(output_folder, make_output_filename(input_file))
    profile_tree.write(output_file)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Splits a SIT profile into multiple files as a basis for inputs '
                                                 'to build_SIT_profile.py')
    parser.add_argument("--profile", '-p', help='Input profile path', default="", required=True)
    args = parser.parse_args()

    split_SIT_profile(args.profile)
