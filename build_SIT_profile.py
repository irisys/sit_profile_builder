from xml.etree import ElementTree
import re
import os
import argparse
from copy import deepcopy


__author__ = "InfraRed Integrated Systems Ltd."
__copyright__ = "Copyright (C) 2017, InfraRed Integrated Systems Ltd."
__license__ = "Irisys Software License Agreement"


def increment_version(version):
    if str.isdigit(version):
        version = str(int(version) + 1)
    else:
        raise Exception("Unable to increment version: " + version)
    return version


def increment_description(description):
    m = re.match(r"(.*)(SPF[\d]{5})([\d]{2})(.*)", description)
    new_spf = ""
    if m is not None and m.group(2) is not None and m.group(3) is not None:
        version_part = m.group(3)
        if version_part == "99":
            new_spf = m.group(2) + m.group(3)
            print("SPF version too high to increment: " + description)
            print("Continuing anyway")
        else:
            new_spf = m.group(2) + "{:02d}".format(int(version_part) + 1)
            description = m.group(1) + new_spf + m.group(4)
    else:
        print("SPF number not found in description: " + description)
        print("Ignoring and continuing")
    return description, new_spf


def increment_customer_summary(customer_summary):
    customer_old_version = customer_summary.attrib["Version"]
    new_version = increment_version(customer_old_version)
    customer_summary.attrib["Version"] = new_version

    customer_old_description = customer_summary.attrib["Description"]
    new_description, new_spf = increment_description(customer_old_description)
    customer_summary.attrib["Description"] = new_description

    return new_version, new_spf


def make_part_number(part_number, customer_number):
    if part_number.startswith('IRC'):
        return part_number

    m = re.match(r"(IRQ\w)(X)(.*)", part_number)
    if m is not None and m.group(1) is not None and m.group(3) is not None:
        part_number = m.group(1) + customer_number + m.group(3)
    else:
        raise Exception("Unable to build part number from: " + part_number)
    return part_number


def add_hash_to_file(output_file):
    if not os.path.isfile("ProfileCreator.exe"):
        raise Exception("ProfileCreator.exe was not found")

    os.system("ProfileCreator.exe -addHash {0} {0}".format(output_file))


def pull_global(global_file, output_root):
    if not os.path.isfile(global_file):
        raise Exception("SIT_profile_global.xml was not found")

    global_root = ElementTree.parse(global_file).getroot()
    global_engine_version = global_root.find("EngineVersion").text

    output_root.find("Summary").attrib["EngineVersion"] = global_engine_version


def pull_part_number_registry(part_number_registry_folder, output_part_number_root, customer_number):
    if customer_number == "?":
        raise Exception("CustomerNumber has not been set in customer xml file")

    if not os.path.isdir(part_number_registry_folder):
        raise Exception("PartNumberRegistry directory was not found")

    for part_number_file in os.listdir(part_number_registry_folder):
        if os.path.splitext(part_number_file)[1] == ".xml":
            part_number_root = ElementTree.parse(os.path.join(part_number_registry_folder, part_number_file)).getroot()
            for part_number_registry_entry in part_number_root:
                part_numbers = part_number_registry_entry.find("PartNumbers")
                for string_val in part_numbers:
                    new_part_number = make_part_number(string_val.text, customer_number)
                    string_val.text = new_part_number
                output_part_number_root.append(part_number_registry_entry)


def pull_checkout_profile(checkout_profile_folder, output_checkout_profiles_root):
    if not os.path.isdir(checkout_profile_folder):
        raise Exception("CheckoutProfiles directory was not found")

    for checkout_file in os.listdir(checkout_profile_folder):
        if os.path.splitext(checkout_file)[1] == ".xml":
            checkout_profile_root = ElementTree.parse(os.path.join(checkout_profile_folder, checkout_file)).getroot()
            for checkout_profile_entry in checkout_profile_root:
                output_checkout_profiles_root.append(checkout_profile_entry)


def make_output_path(path, spf_number):
    file_dir, file_name = os.path.split(path)
    if spf_number:
        file_name = spf_number + "_" + file_name
    else:
        file_name = "New_" + file_name
    new_path = os.path.join(file_dir, file_name)
    return new_path


def build_SIT_profile(customer_file, inputs_folder):
    if inputs_folder == "":
        inputs_folder = os.path.split(customer_file)[0]

    checkout_profiles_folder = os.path.join(inputs_folder, "CheckoutProfiles")
    part_number_registry_folder = os.path.join(inputs_folder, "PartNumberRegistry")

    output_root = ElementTree.Element("SITProfile")

    customer_tree = ElementTree.parse(customer_file)
    customer_root = customer_tree.getroot()

    customer_number = customer_root.find("CustomerNumber").text

    customer_summary = customer_root.find("Summary")

    new_version, new_spf = increment_customer_summary(customer_summary)

    output_root.extend(deepcopy(customer_root))
    output_root.remove(output_root.find("CustomerNumber"))

    global_file = "SIT_profile_global.xml"
    pull_global(global_file, output_root)

    output_part_number_root = ElementTree.Element("PartNumberRegistry")
    pull_part_number_registry(part_number_registry_folder, output_part_number_root, customer_number)
    output_root.append(output_part_number_root)

    output_checkout_profiles_root = ElementTree.Element("CheckoutProfiles")
    pull_checkout_profile(checkout_profiles_folder, output_checkout_profiles_root)
    output_root.append(output_checkout_profiles_root)

    output_tree = ElementTree.ElementTree(output_root)
    output_file = make_output_path(customer_file, new_spf)
    output_tree.write(output_file, encoding="utf-8", xml_declaration=True)
    print("Created file: " + output_file)

    customer_tree.write(customer_file, encoding="utf-8", xml_declaration=True)
    print("Amended file: " + customer_file)

    add_hash_to_file(output_file)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Builds a SIT profile from many separate parts',
                                     epilog='SIT_profile_global.xml and ProfileCreator.exe must be in this directory')
    parser.add_argument("--customer_file", '-c',
                        help='Customer xml file: customer-specific information; read-write', required=True)
    parser.add_argument("--inputs_folder", '-i',
                        help='Inputs folder: contains CheckoutProfiles folder, PartNumberRegistry folder; read-only. '
                             'If not specified, defaults to the directory containing the customer xml file',
                        required=False, default="")
    args = parser.parse_args()

    try:
        build_SIT_profile(args.customer_file, args.inputs_folder)
    except Exception as ex:
        print(str(ex))
        print("Aborted")
